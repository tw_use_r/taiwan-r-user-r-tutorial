台灣地圖資訊呈現 by https://github.com/ronnywang/twmap
========================================================

```{r data, results='asis'}
source("toJSON.data.frame.R")
vote <- read.csv("2012_tw_president_vote.csv")
# index <- sample(1:nrow(vote), 100, FALSE)
# vote <- vote[index,]
cat(sprintf("<script>var Rdata = %s;</script>\n", toJSON.data.frame(vote)))
```


<script src="js/d3.v3.min.js"></script>
<script src="js/queue.v1.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="twmap.js"></script>
<div id="info" style="position: absolute; left: 600px; top: 600px"></div>
<script><!--
var sim_link = 'maplevel=village2010&county_column=0&town_column=1&village_column=2&maptype=2&value1=5&color1=0%2C0%2C255&value2=4&color2=0%2C255%2C0&title=總統大選分佈圖';
var terms = sim_link.split('&');
var params = {};
for (var i = 0; i < terms.length; i ++) {
    params[terms[i].split('=')[0]] = decodeURIComponent(terms[i].split('=')[1]);
}
$('#title').text(params.title).attr({
    href: 'https://www.google.com/fusiontables/DataSource?docid=' + encodeURIComponent(params.docid),
    target: '_blank'
});

var all_data = {};
var handleData = function(data){
    var id;
    var rate;
    params.value1_column = data.table.cols[parseInt(params.value1)];
    params.value2_column = data.table.cols[parseInt(params.value2)];
    params.max_value1 = params.min_value1 = false;
    for (var i = 0; i < data.table.rows.length; i ++) {
        if (params.maptype == 1) { // 只有一個數字的話記最大最小值
            params.max_value1 = (false === params.max_value1) ? data.table.rows[i][parseInt(params.value1)] : Math.max(params.max_value1, data.table.rows[i][parseInt(params.value1)]);
            params.min_value1 = (false === params.min_value1) ? data.table.rows[i][parseInt(params.value1)] : Math.min(params.min_value1, data.table.rows[i][parseInt(params.value1)]);
        } else {
            rate = 1.0 * parseInt(data.table.rows[i][parseInt(params.value1)]) / (parseInt(data.table.rows[i][parseInt(params.value1)]) + parseInt(data.table.rows[i][parseInt(params.value2)]));
            params.max_value1 = (false === params.max_value1) ? rate : Math.max(rate, params.max_value1);
            params.min_value1 = (false === params.min_value1) ? rate : Math.min(rate, params.min_value1);
        }

        id = [];
        id.push(data.table.rows[i][parseInt(params.county_column, 10)]);
        if (!params.maplevel.match('county')) {
            id.push(data.table.rows[i][parseInt(params.town_column, 10)]);
        }
        if (params.maplevel.match('village')) {
            id.push(data.table.rows[i][parseInt(params.village_column, 10)]);
        }
        id = id.join('-');

        d = {};
        for (var j = 0; j < data.table.cols.length; j ++) {
            d[data.table.cols[j]] = data.table.rows[i][j];
        }
        all_data[id] = d;
    }

    var getIDFromProperties = function(properties){
        var ids = [];
        if (properties.county) {
            ids.push(properties.county);
        }
        if (properties.town) {
            ids.push(properties.town);
        }
        if (properties.village) {
            ids.push(properties.village);
        }
        return ids.join('-');
    };
    twmap(params.maplevel + '.json', false, false, {
        style_cb: function(d){
            var map_id = getIDFromProperties(d.properties);
            if ('undefined' == typeof(all_data[map_id])) {
                return 'fill:white';
            }
            if (parseInt(params.maptype) == 2) { // 2 number
                var rate = 1.0 * parseInt(all_data[map_id][params.value1_column]) / (parseInt(all_data[map_id][params.value1_column]) + parseInt(all_data[map_id][params.value2_column]));
                var rgb = [];
                var color1 = params.color1.split(',');
                var color2 = params.color2.split(',');
                if (rate > 0.5) {
                    rate = (rate - 0.5) / (params.max_value1 - 0.5);
                    for (i = 0; i < 3; i ++) {
                        rgb[i] = Math.floor(parseInt(color1[i]) - (parseInt(color1[i]) - 255) * (1 - rate));
                    }
                } else {
                    rate = (0.5 - rate) / (0.5 - params.min_value1);
                    for (i = 0; i < 3; i ++) {
                        rgb[i] = Math.floor(parseInt(color2[i]) - (parseInt(color2[i]) - 255) * (1 - rate));
                    }
                }
                return 'fill:rgb(' + rgb.join(',') + ')';
            } else {
                var rate = 1.0 * parseInt(all_data[map_id][params.value1_column]) / (params.max_value1);
                var rgb = [];
                var color1 = params.color1.split(',');
                for (i = 0; i < 3; i ++) {
                    rgb[i] = Math.floor(255 - (255 - parseInt(color1[i])) * rate);
                }
                return 'fill:rgb(' + rgb.join(',') + ')';
            }
        },
        mouseover_cb: function(e){
            var map_id = getIDFromProperties(e.properties);

            var data = all_data[map_id];
            if ('undefined' == typeof(data)) {
                $('#info').text('不知道的鄉鎮: ' + map_id);
            }
            var body = '';
            for (var i in data) {
                if ('' !== data[i]) {
                    body += i + ': ' + data[i] + '<br>';
                }
            }
            $('#info').html(body);
        }
    });
}

handleData(Rdata)
</script>

